#!/usr/bin/env racket


;; This file is part of racket-ebuild.

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, package-version 3.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


#lang scribble/manual

@(require
  @(only-in scribble/bnf nonterm)
  @(for-label
    racket
    ebuild/tools/pkgname
    )
  )


@title[#:tag "ebuild-tools-pkgname"]{Ebuild Tool - PKGName}


@defmodule[ebuild/tools/pkgname]


@section{About PKGName}

Shows package name for a given directory.


@section{PKGName Console usage}

@itemlist[
 @item{
  @Flag{h} or @DFlag{help}
  --- show help information with usage options
 }
 @item{
  @Flag{V} or @DFlag{verbose}
  --- show the version of this program
 }
 ]

Also takes any number of of arguments not followed by flags
that specify system paths to be passed to "pkgname".
