#!/usr/bin/env racket


;; This file is part of racket-ebuild.

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


#lang typed/racket/base

(require
 (only-in racket/string string-split)
 )

(provide (all-defined-out))


(define (string~= [strA : String] [strB : String]) : Boolean
  (equal? (string-downcase strA) (string-downcase strB))
  )


(define (yesno [description : String]) : Boolean
  (display (string-append description " [y/N] ... "))
  {define inp  (read-line)}
  (cond
    [(and (string? inp) (or (string~= inp "y") (string~= inp "yes")))  #t]
    [else  #f]
    ))

(define (input [name : String]) : String
  (printf "Input ~a ... " name)
  {define inp  (read-line)}
  (if (eof-object? inp)
      ""
      inp
      ))

(define (any-input [name : String] [proc : (-> String Any)]) : Any
  (printf "Input ~a ... " name)
  {define inp  (read-line)}
  (if (or (eof-object? inp) (equal? inp ""))
      #f
      (proc inp)
      ))

(define (yesno-input [name : String] [description : String]) : (U False String)
  (if (yesno description)
      (input name)
      #f
      ))

(define (yesno-split [name : String] [description : String]) : (Listof String)
  (if (yesno description)
      (string-split (input name))
      '()
      ))
