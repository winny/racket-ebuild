#!/usr/bin/env racket


;; This file is part of racket-ebuild.

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


#lang racket/base

(require
 racket/class
 racket/contract
 xml
 "private/metadata/elsenull.rkt"
 "private/metadata/pkgmetadata.rkt"  ; CONSIDER: do we want to export this?
 ;; exported
 "private/metadata/empty-tags.rkt"
 "private/metadata/longdescription.rkt"
 "private/metadata/maintainer.rkt"
 "private/metadata/slots.rkt"
 "private/metadata/upstream.rkt"
 "private/metadata/use.rkt"
 )

(provide
 empty-tag-shorthand
 (contract-out
  [metadata%
   (class/c
    (init-field
     [maintainers          (listof maintainer?)]
     [longdescriptions     (listof longdescription?)]
     [slots                (listof slots?)]
     [stabilize-allarches  boolean?]
     [uses                 (listof use?)]
     [upstream             (or/c #f upstream?)]
     )
    [create       (->m document?)]
    [save         (->*m () (path-string?) void)]
    [custom-display  (->m output-port? void)]
    [custom-print    (->m output-port? integer? void)]
    [custom-write    (->m output-port? void)]
    )]
  )
 (all-from-out "private/metadata/empty-tags.rkt")
 (all-from-out "private/metadata/longdescription.rkt")
 (all-from-out "private/metadata/maintainer.rkt")
 (all-from-out "private/metadata/slots.rkt")
 (all-from-out "private/metadata/upstream.rkt")
 (all-from-out "private/metadata/use.rkt")
 )


;; TODO: parameterize right functions
(empty-tag-shorthand (metadata-empty-tags))


(define metadata%
  (class* object% (printable<%>)
    (super-new)
    (init-field
     [maintainers          '()]
     [longdescriptions     '()]
     [slots                '()]
     [stabilize-allarches  #f]
     [uses                 '()]
     [upstream             #f]
     )

    (define/public (create)
      (pkgmetadata
       (append
        (map maintainer->xexpr maintainers)
        (map longdescription->xexpr longdescriptions)
        (map slots->xexpr slots)
        (elsenull stabilize-allarches  '((stabilize-allarches)))
        (map use->xexpr uses)
        (elsenull upstream  (list (upstream->xexpr upstream)))
        )
       ))

    (define/public (save [pth (current-directory)])
      (with-output-to-file
        (build-path pth "metadata.xml")
        (lambda () (displayln this))
        #:exists 'replace
        ))

    ;; Interface Implementation: printable<%>
    (define/public (custom-display port)
      (display-xml (create) port #:indentation 'scan)
      )
    (define/public (custom-print port quote-depth)
      (print (create) port quote-depth)
      )
    (define/public (custom-write port)
      (write-xml (create) port)
      )

    ))
