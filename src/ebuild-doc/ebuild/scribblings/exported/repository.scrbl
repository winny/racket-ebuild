#!/usr/bin/env racket


;; This file is part of racket-ebuild.

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, package-version 3.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


#lang scribble/manual

@(require
   scribble/example
   @(for-label
     racket
     ebuild
     )
   )


@(define repository-eval
   (make-base-eval '(require ebuild/repository)))


@declare-exporting[ebuild/repository]


@title[#:tag "ebuild-exported-repository"]{Repository Functions}


@section{Layout}

@defstruct[
 layout
 (
  [masters                   string?]
  [cache-formats             (listof string?)]
  [sign-commits              boolean?]
  [update-changelog          boolean?]
  [eapis-banned              (listof exact-integer?)]
  [eapis-deprecated          (listof exact-integer?)]
  [manifest-hashes           (listof string?)]
  [manifest-required-hashes  (listof string?)]
  [sign-manifests            boolean?]
  [thin-manifests            boolean?]
  )
 ]{
}

@defparam[
 default-layout layout layout?
 #:value
 (layout
  "gentoo"
  '("md5-dict")
  #t
  #f
  '(0 1 2 3 4 5 6)
  '()
  '("BLAKE2B" "SHA512")
  '("BLAKE2B")
  #f
  #t
  )
 ]{
 Parameter that determines default @racket[layout] used when creating a
 @racket[repository%] object.
}

@defproc[
 (layout->string
  [lo  layout?]
  )
 string?
 ]{
 Converts @racket[lo layout struct] to a @racket[string].

 @examples[
 #:eval repository-eval
 (display (layout->string (default-layout)))
 ]
}
