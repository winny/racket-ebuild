#!/usr/bin/env racket


;; This file is part of racket-ebuild.

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


;; Test the cargo.rkt template. Loosely based on ripgrep ebuild.


#lang racket/base


(module+ test
  (require
   rackunit
   racket/class
   (only-in racket/file file->string)
   (only-in racket/port with-output-to-string)
   ebuild/ebuild
   ebuild/templates/cargo
   )


  (define ripgrep-bare
    (new
     ebuild-cargo%
     [year  2021]
     [HOMEPAGE  "https://github.com/BurntSushi/ripgrep"]
     ))
  (define ripgrep
    (new
     ebuild-cargo%
     [year  2021]
     [CRATES    '("aho-corasick-0.7.10" "atty-0.2.14" "autocfg-1.0.0")]
     [inherits  '("bash-completion-r1")]
     [HOMEPAGE  "https://github.com/BurntSushi/ripgrep"]
     [SRC_URI   `(,(src-uri
                    #f
                    "https://github.com/BurntSushi/${PN}/archive/${PV}.tar.gz"
                    "${P}.tar.gz"
                    ))]
     [LICENSE   "Apache-2.0 BSD-2 Boost-1.0 || ( MIT Unlicense )"]
     ))


  (check-equal?  (with-output-to-string (lambda () (displayln ripgrep-bare)))
                 (file->string "ripgrep-bare.ebuild"))
  (check-equal?  (with-output-to-string (lambda () (displayln ripgrep)))
                 (file->string "ripgrep.ebuild"))
  )
