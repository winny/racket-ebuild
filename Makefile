# This file is part of racket-ebuild.

# racket-ebuild is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.

# racket-ebuild is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with racket-ebuild.	 If not, see <https://www.gnu.org/licenses/>.

# Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v3 License
# SPDX-License-Identifier: GPL-3.0-only


MAKE		:= make
RACKET		:= racket
RACO		:= raco
SCRIBBLE	:= $(RACO) scribble
SH			:= sh

# For recursive calls
WHAT		:=


all: compile


# Targets from src/Makefile (some exactly "translated", some modified)


ebuild-make:
	cd ./src && $(MAKE) DEPS-FLAGS=" --no-pkg-deps " $(WHAT)


ebuild-clean:
	$(MAKE) ebuild-make WHAT=clean

clean: ebuild-clean bin-clean public-clean


ebuild-compile:
	$(MAKE) ebuild-make WHAT=compile

compile: ebuild-compile


ebuild-install:
	$(MAKE) ebuild-make WHAT=install

install: ebuild-install


ebuild-setup:
	$(MAKE) ebuild-make WHAT=setup

setup: ebuild-setup


ebuild-test:
	$(MAKE) ebuild-make WHAT=test

test-full: ebuild-test


ebuild-remove:
	$(MAKE) ebuild-make WHAT=remove

remove: ebuild-remove


ebuild-purge:
	$(MAKE) ebuild-make WHAT=purge

purge: ebuild-purge


# Wrappers for scripts


bin:
	$(RACKET) ./scripts/exes.rkt

bin-clean:
	rm -dfr ./bin

bin-regen: bin-clean bin


test-unit:
	$(SH) ./scripts/test.sh unit

test-integration:
	$(SH) ./scripts/test.sh integration

test: test-unit test-integration


public:
	$(SH) ./scripts/public.sh

public-clean:
	rm -dfr ./public

public-regen: public-clean public
