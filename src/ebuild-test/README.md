# API Tests


Public API should have tests here.

Private API that is not re-exported by any public module
can have tests in the same file.
