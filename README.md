# Racket-Ebuild

<p align="center">
    <a href="http://pkgs.racket-lang.org/package/ebuild">
        <img src="https://img.shields.io/badge/raco_pkg_install-ebuild-aa00ff.svg">
    </a>
    <a href="https://archive.softwareheritage.org/browse/origin/?origin_url=https://gitlab.com/xgqt/racket-ebuild">
        <img src="https://archive.softwareheritage.org/badge/origin/https://gitlab.com/xgqt/racket-ebuild/">
    </a>
    <a href="https://gitlab.com/xgqt/racket-ebuild/pipelines">
        <img src="https://gitlab.com/xgqt/racket-ebuild/badges/master/pipeline.svg">
    </a>
</p>

Library to ease automatic ebuild creation.

![racket-ebuild-logo](./src/ebuild-doc/ebuild/scribblings/assets/img/racket-ebuild-logo.svg "Racket Ebuild logo (CC-BY-SA/2.5 license)")


## About

This package is meant to help [Gentoo](https://gentoo.org/)
(and it's forks) developers
in creating [ebuilds](https://wiki.gentoo.org/wiki/Ebuild).

It is primarily made with
[collector2](https://gitlab.com/src_prepare/racket/collector2) in mind.


### Online Documentation

You can read more documentation on
[GitLab pages](https://xgqt.gitlab.io/racket-ebuild/).


### Subpackages

| name                | role                                                    |
|---------------------|---------------------------------------------------------|
| ebuild              | metapackage, re-exports lib, transformers and templates |
| ebuild-doc          | documentation of the whole project                      |
| ebuild-lib          | core library                                            |
| ebuild-templates    | additional ebuild templates                             |
| ebuild-test         | tests of exported API and tools                         |
| ebuild-tools        | command-line programs using ebuild-lib                  |
| ebuild-transformers | helpers transforming miscellaneous data for ebuilds     |


### Installation

#### From Packages Catalog

Requesting `ebuild` installation should pull in all subpackages.
```sh
raco pkg install ebuild
```

#### From repository

```sh
(cd ./src && make install)
```


## Dependencies

Packages without links should already be in your version of main Racket distribution.

| name                                                               | required                |
|--------------------------------------------------------------------|-------------------------|
| [upi-lib](https://gitlab.com/xgqt/racket-upi)                      | yes                     |
| [threading-lib](https://github.com/lexi-lambda/threading)          | yes                     |
| [repoman](https://gitweb.gentoo.org/proj/portage.git/tree/repoman) | no (optional for tests) |
| base                                                               | yes                     |
| racket-doc                                                         | yes (documentation)     |
| rackunit-lib                                                       | yes (tests)             |
| rackunit-typed                                                     | yes (tests)             |
| scribble-lib                                                       | yes (documentation)     |
| typed-racket-lib                                                   | yes                     |


## License

SPDX-License-Identifier: GPL-3.0-only

This file is part of racket-ebuild.

racket-ebuild is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

racket-ebuild is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.

Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
Licensed under the GNU GPL v3 License

The "racket-ebuild-logo.svg" is licensed under the CC-BY-SA/2.5 license.
