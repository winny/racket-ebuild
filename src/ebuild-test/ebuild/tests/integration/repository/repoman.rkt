#!/usr/bin/env racket


;; This file is part of racket-ebuild.

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


#|
racket-ebuild
└── test/
    └── repository/
        └── repoman/
            ├── app-misc/
            │   └── test-pkg/
            │       ├── metadata.xml
            │       └── test-package-9999.ebuild
            ├── metadata/
            │   └── layout.conf
            └── profiles/
                └── repo_name
|#

;; If this doesn't fail then it's good


#lang racket/base


(module+ test
  (require
   racket/file
   racket/system
   rackunit
   ebuild
   )


  (define tmp-dir
    (build-path (find-system-path 'temp-dir) "racket-ebuild"))
  (define test-dir
    (build-path tmp-dir "test" "repository" "repoman"))

  (define (test-repoman)
    (current-directory test-dir)
    (displayln (current-directory))

    (define test-eb
      (new ebuild%
           [inherits '("git-r3")]
           ))
    (define test-meta
      (new metadata%
           [maintainers (list (maintainer 'project #f "null@gentoo.org" #f #f))]
           ))
    (define test-pkg
      (new package%
           [CATEGORY  "sys-devel"]
           [PN        "test-pkg"]
           [ebuilds   (hash (live-version) test-eb)]
           [metadata  test-meta]
           ))
    (define test-repo
      (new repository%
           [name      "test-repo"]
           [packages  (list test-pkg)]
           ))
    (send test-repo save test-dir)

    (system "repoman -Idx full")
    )


  (when (find-executable-path "repoman")
    (check-not-false  (test-repoman))
    ;; cleanup
    (delete-directory/files tmp-dir)
    )
  )
