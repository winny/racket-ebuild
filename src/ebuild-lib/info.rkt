#!/usr/bin/env racket


;; This file is part of racket-ebuild.

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


#lang info


(define pkg-desc
  "Library to ease automatic ebuild creation. Core.")
(define version "9.0.0")

(define pkg-authors '(xgqt))
(define license '(GPL-3.0-only))

(define collection 'multi)

(define deps
  '(
    "base"
    "typed-racket-lib"
    "threading-lib"
    ))
(define build-deps
  '(
    "rackunit-lib"
    "rackunit-typed"
    ))
