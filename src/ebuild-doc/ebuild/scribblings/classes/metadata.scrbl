#!/usr/bin/env racket


;; This file is part of racket-ebuild.

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, package-version 3.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


#lang scribble/manual

@(require
   scribble/example
   @(for-label
     racket
     racket/class
     xml
     ebuild
     )
   )


@(define metadata-eval
   (make-base-eval '(require racket/class ebuild/metadata)))


@title[#:tag "ebuild-classes-metadata"]{Metadata Class}


@defmodule[ebuild/metadata]

@defclass[
 metadata% object% (printable<%>)
 ]{
 Metadata class.

 For crating package
 @link["https://devmanual.gentoo.org/ebuild-writing/misc-files/metadata/"
       "metadata"]
 files (@filepath{metadata.xml}).
 @defconstructor[
 (
  [maintainers          (listof maintainer?)       '()]
  [longdescriptions     (listof longdescription?)  '()]
  [slots                (listof slots?)            '()]
  [stabilize-allarches  boolean?                   #f]
  [uses                 (listof use?)              '()]
  [upstream             (or/c #f upstream?)        #f]
  )
 ]{
 }

 @defmethod[
 (create)
 document?
 ]{
  Creates a XML @racket[document] ready to be written into a file.
 }

 @defmethod[
 (save
  [pth path-string? (current-directory)]
  )
 void
 ]{
  Creates a file named @filepath{metadata.xml}
  in the given location (or current directory).
  Internally uses the interfece implemented by this object's
  @racket[printable<%>] to dispaly object to file.
 }

 @examples[
 #:eval metadata-eval
 (define my-metadata
   (new metadata%
        [maintainers
         (list (maintainer 'person #f "me@me.com" "Me" #f))]
        [upstream
         (upstream (list) #f #f #f (list (remote-id 'gitlab "me/myproject")))]
        ))
 (display my-metadata)
 ]

}
