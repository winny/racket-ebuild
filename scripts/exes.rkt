#!/usr/bin/env racket


;; This file is part of racket-ebuild.

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


#lang racket/base

(require
 racket/file
 racket/system
 upi/basename
 )


(module+ main

  {define bindir (simplify-path "./bin")}
  {define excluded '("compiled" "info" "private")}

  (delete-directory/files #:must-exist? #f bindir)
  (make-directory* bindir)

  (for-each
   (lambda (f) {define exe (regexp-replace ".rkt" (basename f) "")}
     (when (not (member exe excluded))
       (system* (find-executable-path "raco") "exe" "-v" "--orig-exe" "-o"
                (path->string (build-path bindir exe)) (path->string f))
       ))
   (directory-list #:build? #t "./src/ebuild-tools/ebuild/tools/")
   )

  )
