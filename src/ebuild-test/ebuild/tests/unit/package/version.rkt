#!/usr/bin/env racket


;; This file is part of racket-ebuild.

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


#lang racket/base


(module+ test
  (require
   rackunit
   ebuild/package
   )


  (for ([c '("1" "1" "111" "1.1.1" "1.1.1a" "111a")])
    (check-true  (release? c))
    )
  (for ([c '("a" "1a1" "a1a" "ab" "111ab")])
    (check-false  (release? c))
    )

  (check-equal?  (package-version->string (live-version))
                 (package-version->string (package-version "9999" #f #f #f #f #f)))

  (define-values (v1 v2 v3) (values
                             (package-version "1.2.3" #f #f #f #f #f)
                             (package-version "1.2.3" 'b #f #f #f #f)
                             (package-version "1.2.3" 'a 4  5  6  7)
                             ))
  (check-equal?  (package-version-release v1)  "1.2.3")
  (check-equal?  (package-version-phase v1)    #f)
  (check-equal?  (package-version->string v1)  "1.2.3")
  (check-equal?  (package-version-phase v2)    'b)
  (check-equal?  (package-version->string v3)  "1.2.3_alpha_pre4_rc5_p6-r7")
  )
