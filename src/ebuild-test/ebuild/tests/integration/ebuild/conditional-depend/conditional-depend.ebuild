# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="package"
HOMEPAGE="https://wiki.gentoo.org/wiki/No_homepage"

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="~amd64"

RDEPEND="category/name1
	category/name2
	category/name3
	|| ( category/name4
		category/name5 )
	debug? ( category/name6 )
	test? ( category/name7
		category/name8
		category/name9 )"
DEPEND="${RDEPEND}"
