#!/usr/bin/env racket


;; This file is part of racket-ebuild.

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


#lang racket/base

(require
 threading
 (only-in racket/string string-join)
 "constraint-flag.rkt"
 "variable.rkt"
 )

(provide (all-defined-out))


(define-syntax-rule (unroll-list lst-id)
  (if (null? lst-id)  #f
      (as-variable (symbol->string 'lst-id) (string-join lst-id))
      ))

(define-syntax-rule (unroll-DEPEND lst-id)
  (if (null? lst-id)  #f
      (~> lst-id
          (map (lambda (v) (if (cflag? v) (cflag->string v 2) v)) _)
          (string-join _ "\n\t")
          (as-variable (symbol->string 'lst-id) _)
          )
      ))


(module+ test

  (require rackunit)

  {define zero '()}
  {define one  '("asd/asd")}
  {define two  '("fgh/fgh" "jkl/jkl")}

  (check-equal?  (unroll-list zero)  #f)
  (check-equal?  (unroll-list one)   "one=\"asd/asd\"")
  (check-equal?  (unroll-list two)   "two=\"fgh/fgh jkl/jkl\"")

  (check-equal?  (unroll-DEPEND zero)  #f)
  (check-equal?  (unroll-DEPEND one)   "one=\"asd/asd\"")
  (check-equal?  (unroll-DEPEND two)   "two=\"fgh/fgh\n\tjkl/jkl\"")

  {define zerof '()}
  {define onef  `(,(cflag "z?" '("z/z1" "z/z2")))}
  {define twof  `(,(cflag "||" `("x/x" ,@onef)))}

  (check-equal?  (unroll-DEPEND zerof)  #f)
  (check-equal?  (unroll-DEPEND onef)   "onef=\"z? ( z/z1\n\t\tz/z2 )\"")
  (check-equal?  (unroll-DEPEND twof)
                 "twof=\"|| ( x/x\n\t\tz? ( z/z1\n\t\t\tz/z2 ) )\"")

  )
