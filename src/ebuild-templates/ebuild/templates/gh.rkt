#!/usr/bin/env racket


;; This file is part of racket-ebuild.

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


;; gh.eclass - https://gitlab.com/src_prepare/rkt/-/blob/master/eclass/gh.eclass
;; example   - https://gitlab.com/src_prepare/rkt/-/blob/master/dev-racket/collector2/collector2-2021.06.25.ebuild


#lang ebuild

(require
 racket/contract
 )

(provide
 ebuild-gh-mixin
 (contract-out
  [ebuild-gh%
   (class/c
    (init-field
     [GH_REPO    string?]
     [GH_DOM     string?]
     [GH_COMMIT  (or/c #f string?)]
     ))])
 )


(define (ebuild-gh-mixin %)
  (class %
    (init-field
     GH_REPO     ; required
     [GH_DOM     "gitlab.com"]
     [GH_COMMIT  #f]
     )
    (super-new)
    (inherit-field HOMEPAGE)

    ;; Add "gh" to inherited eclasses
    (ebuild-concat! inherits this "gh")

    ;; Use a automatically generated website if HOMEPAGE is empty
    (when (equal? "" HOMEPAGE)
      (set! HOMEPAGE (string-append "https://" GH_DOM "/" GH_REPO))
      )

    (define (unroll-GH)
      (string-append
       (make-variable GH_DOM) "\n"
       (make-variable GH_REPO)
       (if GH_COMMIT  (string-append "\n" (make-variable GH_COMMIT))  "")
       ))
    (ebuild-concat! custom this unroll-GH)
    ))


(define ebuild-gh%
  (ebuild-gh-mixin ebuild%)
  )


#|
(displayln (new ebuild-gh% [GH_REPO "asd/asd"] [GH_DOM "gitlab.com"] [GH_COMMIT "0"]))
|#
