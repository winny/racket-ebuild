#!/usr/bin/env racket


;; This file is part of racket-ebuild.

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


#lang typed/racket/base


(module+ test

  (require
   typed/rackunit
   typed/net/url
   ebuild/transformers/githost
   )

  (define c
    (url "https" #f "asd.com" #f #t
         (list (path/param "source" '()) (path/param "asd-project" '())
               (path/param "asd.git" '()))
         '() #f))
  (define cs (url->string c))
  (define cg (githost "asd.com" "source/asd-project/asd"))

  (check-equal?  cs  "https://asd.com/source/asd-project/asd.git")
  (check-equal?  cs (string-append (githost->string cg) ".git"))
  (check-not-false  cs)

  )
