#!/usr/bin/env racket


;; This file is part of racket-ebuild.

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, package-version 3.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


#lang scribble/manual

@(require
  @(for-label
    racket
    ebuild
    ebuild/transformers/uncache
    )
  )


@title[#:tag "ebuild-transformers-uncache"]{UnCache}

@defmodule[ebuild/transformers/uncache]


@defproc[
 (uncache
  [pth  path-string?]
  )
 ebuild%
 ]{
 Attempts to convert a cached ebuild file at @racket[pth] to
 a @racket[ebuild%] (racket object).

 Certain to be missed are any additional shell script functions
 because cached ebuild files contain only special ebuild variables.
}
