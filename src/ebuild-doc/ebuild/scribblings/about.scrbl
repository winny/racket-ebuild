#!/usr/bin/env racket


;; This file is part of racket-mike.

;; racket-mike is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, package-version 3.

;; racket-mike is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-mike.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


#lang scribble/manual


@title[#:tag "ebuild-about"]{About}


@section{Project Aim}

Racket-Ebuild is primarily made with
@link["https://gitlab.com/src_prepare/racket/collector2" "collector2"]
in mind.

This package is meant to help @link["https://gentoo.org/" "Gentoo"]
(and it's forks) developers in creating and maintaining
@link["https://wiki.gentoo.org/wiki/Ebuild" "ebuilds"].


@section{Development}

@subsection{Tools}

@itemlist[
 @item{@link["https://wiki.gentoo.org/wiki/Handbook:Main_Page"
             ]{Gentoo GNU+Linux}
  system (for testing generated ebuilds) with following tools:
  @itemlist[@item{@link["https://packages.gentoo.org/packages/app-portage/repoman"
                        ]{repoman}
               --- for verifying ebuild QA correctness}
            @item{@link["https://packages.gentoo.org/packages/app-emacs/ebuild-mode"
                        ]{ebuild-mode}
               --- if using GNU Emacs for viewing or manually editing ebuilds}
            ]}
 @item{IDE/editor with @link["https://editorconfig.org/"
                             ]{editorconfig}
  support (or just follow rules in @filepath{.editorconfig} file)}
 @item{DrRacket for formatting Scribble code
  (and writing Racket code in general if it is your editor of choice)}
 @item{@link["https://digdeeper.neocities.org/ghost/browsers.html"
             ]{Web browser}
  for reading documentation (generated from Scribble code)}
 ]

@subsection{Weirdness}

@itemlist[
 @item{
  Besides non-lisp style and sometimes weird indentation...
 }
 @item{
  "ebuild" and "metadata" are implemented as
  @link["https://docs.racket-lang.org/guide/classes.html"
        "classes"]
 }
 @item{
  This is one repository that has many packages
  (so-called monorepo)
 }
 ]


@section{Upstream}

The upstream repository can be found on
@link["https://gitlab.com/xgqt/racket-ebuild" "GitLab"].

GitLab allows to run
@link["https://gitlab.com/xgqt/racket-ebuild/-/pipelines"]{CI pipelines}
and to generate @link["https://xgqt.gitlab.io/racket-ebuild/"]{
 static web pages for documentation}.

Configuration for GitLab CI/CD pipelines can be found in
@link["https://gitlab.com/xgqt/racket-ebuild/-/blob/master/.gitlab-ci.yml"]{
 .gitlab-ci.yml}.


@section{License}

Racket-Ebuild is released under GNU GPL, version 3 (only) license.

Read the
@link["https://spdx.org/licenses/GPL-3.0-only.html" "license text here"].
