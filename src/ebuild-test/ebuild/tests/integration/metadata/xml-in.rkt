#!/usr/bin/env racket


;; This file is part of racket-ebuild.

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


#lang racket/base


(module+ test

  (require
   rackunit
   racket/class
   racket/file
   racket/port
   ebuild/metadata
   )

  (define c0
    (new
     metadata%
     [maintainers (list (maintainer 'person #f "z@z.z" "Z Z" #f)
                        (maintainer 'person 'yes "x@x.x" "X X" "X")
                        (maintainer 'project #f "null@gentoo.org" #f #f))]
     [longdescriptions (list (longdescription "en" "pkg description"))]
     [slots (list (slots #f (list (slot "1" "lib.so.1") (slot "2" "lib.so.2"))
                         "some ABI"))]
     [stabilize-allarches #t]
     [uses (list (use #f (list (uflag "a" "enable a") (uflag "b" "enable b"))))]
     [upstream (upstream
                (list (upstreammaintainer 'active  "z@z.z" "Z Z")
                      (upstreammaintainer 'unknown "x@x.x" "X X"))
                "https://asd.com/pkg/changelog.txt"
                "https://asd.com/pkg/doc"
                "https://asd.com/pkg/source"
                (list (remote-id 'github "asd/asd")
                      (remote-id 'gitlab "asd-project/asd"))
                )]
     ))

  (check-not-false  (send c0 create))

  (check-equal?
   (with-output-to-string (lambda () (displayln c0)))
   (file->string "xml-in-c0.metadata.xml")
   )

  )
